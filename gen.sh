#!/bin/bash -ue

help() {
    echo "Usage: $0 [OPTIONS] <TARGET_DIR> [FILES]" >&2
    echo "Generates a web gallery into TARGET_DIR, with the files FILES." >&2
    echo "If FILES is unspecified, read the list of files from stdin." >&2
    echo "Requires ImageMagick for picture resizing." >&2
    echo >&2
    echo "Options:" >&2
    echo "  --noindex	don't copy the index.html to the target directory (to be served from parent dir)" >&2
}

DIR=
NOINDEX=
while [ $# -gt 0 ]; do
    case "$1" in
        -h|--help) help; exit 0;;
        --noindex) NOINDEX=1;;
        -*) help; exit 2;;
        *)
            if [ -z "$DIR" ]; then DIR=$1
            else PHOTOS=("$@"); break;
            fi
    esac
    shift
done

if [ -z "$DIR" ]; then help; exit 2; fi

if [ ${#PHOTOS[@]} -eq 0 ]; then
    echo "Reading picture list from stdin..." >&2
    readarray -t PHOTOS
fi
OUT="$DIR/photos.json"

mkdir -p "$DIR/large"
mkdir -p "$DIR/thumbs"

echo "[" >"$OUT"
i=0
for p in "${PHOTOS[@]}"; do
    base=$(basename "${p%.*}")
    convert -auto-orient -resize 1920x1920\> -interlace Plane -quality 75% "$p" "$DIR/large/$base.jpg" &
    convert -auto-orient -thumbnail 640x640\> -strip -interlace Plane -quality 64% "$p" "$DIR/thumbs/$base.jpg" &
    read w h orient < <(identify -format '%w %h %[EXIF:Orientation]\n' "$DIR/large/$base.jpg")
    if [ $i -ne 0 ]; then echo "," >>"$OUT"; fi
    case "$orient" in
        1|3|"") RATIO=$(bc -l <<<"$w / $h") ;;
        *) RATIO=$(bc -l <<<"$h / $w")
    esac
    LC_ALL=C printf '  { "data": "large/%s", "src": "thumbs/%s", "ratio": %f }' "$base.jpg" "$base.jpg" "$RATIO" >>"$OUT"
    i=$((i+1))
    printf "\r[KResizing images: %02d%%" $((i * 100 / ${#PHOTOS[*]}))
done
echo

echo >>"$OUT"
echo "]" >>"$OUT"

wait

echo "Gallery generated into $DIR" >&2
# xdg-open "$DIR/index.html"
